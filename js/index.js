$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (e) {
        console.log('el modal se esta mostrando');

        $('#cotizarBtn').removeClass('btn-outline-success');
        $('#cotizarBtn').addClass('btn-primary');
        $('#cotizarBtn').prop('disabled', true);

    });

    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('El modal contacto se esta mostrando')
    });

    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('El modal contacto se oculta')

        $('#cotizarBtn').prop('disabled', false);
        $('#cotizarBtn').addClass('btn-outline-success');
        $('#cotizarBtn').removeClass('btn-primary');

    });

    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('El modal contacto se oculto')
    });

});